apt-get update
apt-get -y install apt-transport-https lsb-release
curl -o public.gpg https://kolanich.gitlab.io/CMake_deb_packages_CI/public.gpg
apt-key add public.gpg
echo deb [arch=amd64,signed-by=0e94c991ee6dfe96affa14a8a059ada6ca7b4a3f] https://kolanich.gitlab.io/CMake_deb_packages_CI/repo $(lsb_release -sc) contrib >> /etc/apt/sources.list.d/vanilla_CMake_KOLANICH.list
apt-get update
apt-get -y install vanilla-cmake ninja-build
