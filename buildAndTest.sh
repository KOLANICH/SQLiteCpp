#!/bin/sh
# Copyright (c) 2012-2019 Sébastien Rombauts (sebastien.rombauts@gmail.com)
#
# Distributed under the MIT License (MIT) (See accompanying file LICENSE.txt
# or copy at http://opensource.org/licenses/MIT)

# exit on firts error
set -e

mkdir -p build
cd build

cat >> ./CMakeCache.txt <<EOF
CMAKE_BUILD_TYPE:STRING=Release
CMAKE_VERBOSE_MAKEFILE:BOOL=ON
BUILD_SHARED_LIBS:BOOL=ON
SQLITECPP_BUILD_EXAMPLES:BOOL=ON
SQLITECPP_BUILD_TESTS:BOOL=OFF
SQLITECPP_RUN_CPPLINT:BOOL=OFF
SQLITECPP_USE_GCOV:BOOL=OFF
EOF

ninjaPath=$(which name_of_executable)
if [ -x "$ninjaPath" ] ; then
    cmake -G Ninja ..
else
    cmake -G "Unix Makefiles" ..
fi
cmake --build .

# Build and run unit-tests (ie 'make test')
ctest --output-on-failure

